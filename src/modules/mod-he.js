/**
 * HE functions
 * 
 * Dependencies: jQuery
 * 
 */
var modHE = function(opt) {


	function improveHE() {
		var cui = this;
		
		function restoreAdminSwitcher() {
			// need to know if authorized
		}
		
		function enhanceDashboard() {
			
			
			
			const uri = '/rs/api/form/submission/summary';
			
			jQuery('.dashboard-filters .dashboard-button').each(function () {
			    var o = jQuery(this);
			    var href=o.attr('href'); // href: submission/list/status=Unsubmitted%2CReopened 
			    o.find('> .basic.segment:not(.iui-styled)').addClass('iui-styled').find('.arrow.icon').before('<span class="iui-count">(...)</span>');

			    var status = href.substring(href.indexOf('=')+1);
			    	//?full=true&sort=createDate&sort.dir=desc&status=Unsubmitted%2CReopened&size=25&page=1&_=1668699547664
			    
			    function refresh () {
			    	jQuery('.dashboard-filters .dashboard-button .iui-count').text('(...)');
			    	var data = {
					    	full: 'true',
					    	sort: 'createDate',
					    	'sort.dir': 'desc',
					    	status: status,
					    	size: 25,
					    	page: 1,
					    	'_': Date.now()
					    }

					    console.log('Refreshing...', status, Date.now());
					    
						jQuery.ajax({
							url : uri,
							data : data,
							dataType : 'text',
						}).done(function(data, textStatus, jqXHR) {

							//console.log(textStatus, jqXHR);
							var r = JSON.parse(data);
							var cnt = r.page.totalElements;
				            
							o.find('.iui-count').text('('+cnt+')');
						});
			    }
			    refresh();
			    setInterval(refresh, 5*60*1000);
			    
			});
		}

		setTimeout(enhanceDashboard, 1000);
		
	}

	const pages = [
			'HE | improveHE | rs/irb | ',
		];
	

	return {
		pages : pages,
		//
		improveHE : improveHE,

	}

}();
