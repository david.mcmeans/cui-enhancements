/**
 * Admin functions
 * 
 * Dependencies: jQuery
 * 
 */
var modAdmin = function(opt) {


	function improveAdminExtOrgs() {
		var cui = this;
		
		function enhanceExtOrgs() {
			// search by name
			jQuery('[data-component$="FacetMenu"] button:contains(Name)').click();
			
		}

		setTimeout(enhanceExtOrgs, 500);
		
	}
	
	function improveAdminUnits() {
		var cui = this;
		
		function enhanceUnits() {
			// search by name
			jQuery('[data-component$="FacetMenu"] button:contains(Name)').click();
			
		}

		setTimeout(enhanceUnits, 500);
		
	}

	function improveAdminPeople() {
		var cui = this;
		
		function enhancePeople() {
			// search by name
			jQuery('[data-component$="FacetMenu"] button:contains(Name)').click();
			
		}

		setTimeout(enhancePeople, 500);
		
	}

	function improveAdminTeams() {
		var cui = this;
		
		function enhanceTeams() {
			// search by name
			jQuery('[data-component$="FacetMenu"] button:contains(Team)').click();
			
		}

		setTimeout(enhanceTeams, 500);
		
	}

	
	const pages = [
			'AdminExtOrgs | improveAdminExtOrgs | admin-web/organizations | ',
			'AdminUnits | improveAdminUnits | admin-web/units | ',
			'AdminPeople | improveAdminPeople | admin-web/people | ',
			'AdminTeams | improveAdminTeams | admin-web/adhocteams | ',
		];
	

	return {
		pages : pages,
		//
		improveAdminExtOrgs : improveAdminExtOrgs,
		improveAdminUnits : improveAdminUnits,
		improveAdminPeople : improveAdminPeople,
		improveAdminTeams : improveAdminTeams,

	}

}();
