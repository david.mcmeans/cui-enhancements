
function initLog(dbgLevel) {

	var log = {
			ALL : 0,
			DEBUG : 1,
			INFO : 2,
			WARN : 3,
			ERROR : 4,
			FATAL : 5,
			OFF : 6,
			LOG_LEVELS : [ 'ALL', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL', 'OFF' ],
			dbgLevel : 2, // default to INFO
			log : function(msgLevel, context, msg) {

				// context is expected to be the function name
				// msgLevel - this log msg should be shown when dbgLevel is up to msgLevel
				if (msgLevel >= log.dbgLevel) {
					// left pad
					var level = ('     ' + log.LOG_LEVELS[msgLevel]).slice(-5);
					console.log(level + ' ' + context + '(): ' + msg);
				}
			},
			info : function(context, msg) {

				log.log(log.INFO, context, msg);
			},
			debug : function(context, msg) {

				log.log(log.DEBUG, context, msg);
			},
			warn : function(context, msg) {

				log.log(log.WARN, context, msg);
			}
	};

	log.dbgLevel = (typeof dbgLevel !== 'undefined') ? dbgLevel : log.INFO;
	console.log('initLog', log);
	
	return log;
}

var cui = function() {
	
	
    var pages = [];
    var rawPageList = [];
    var log;

	
    

	function addModules() {
		var cui =this;
		
		var pageList = [];
		
		const list = [ modHE, modAdmin ];
		list.forEach(function(item, index) {
			cui =jQuery.extend(cui, item);
			if (item.pages) {
				pageList = pageList.concat(item.pages);
			}
			//console.log('pages', index, cui.pages.length, pageList.length);
		});
		cui.rawPageList = pageList;
		cui.pages = parsePageList(pageList);
	}
	
	/**
	 * Parse a list of pages as strings into a list of objects with label, method, uri and notes
	 */
	function parsePageList(pages) {
		var result = [];
		for (var i=0; i<pages.length; i++) {
			var list = pages[i].split('|');
			var page = {
					label:list[0].trim(),
					method:list[1].trim(),
					uri:list[2].trim(),
					notes:list[3].trim(),
			};
			result.push( page );
		}
		return result;
	}


	function getEnhancerForCurrentPage() {
		var cui =this;
		var page = null;

		// search page list for uri matching current href to see if we have an improver
		var href = window.location.href.toLowerCase();
		for (var i=0; i<cui.pages.length; i++) {
			if (href.indexOf(cui.pages[i].uri.toLowerCase()) >=0) {
				page = cui.pages[i];
			}
		}
		return page;
	}

	function logPage(page) {
		var cui =this;

		console.log(page.label);
		//log.info(f, 'Path is ' + location + ', System is ' + cui.system);

		//cui.setEnhanceToggleLabel({pageName:page.label});
	}

	function enhanceCurrentPage() {

		var page = cui.getEnhancerForCurrentPage();

		if (page) {
			cui.logPage(page);
			
           	if (jQuery.isFunction(cui[page.method])) {
           		cui[page.method]();	
           	} else {
           		console.log(cui[page.method],'is not a function!', page);
           	}
		}
	}
	
	

	
	


	function startup(dbgLevel) {
		var cui =this;
		jQuery.noConflict(); // if window.$ has been used by other libs

		cui.log = cui.initLog(dbgLevel);

		cui.addModules();

		let currentUrl = location.href;
		cui.enhanceCurrentPage();
		
		setInterval( () => {
			if (location.href !== currentUrl) {
				currentUrl = location.href;
				
				cui.enhanceCurrentPage();
			}
		}, 500);
	
	}


	return {

		pages : pages,
		rawPageList : rawPageList,
		log : log,
		//
		initLog : initLog,
		startup : startup,

		addModules : addModules,

		enhanceCurrentPage : enhanceCurrentPage,
		getEnhancerForCurrentPage : getEnhancerForCurrentPage,
		logPage : logPage,
		//

	}

}();


jQuery(function() {
	console.log('cui - start');

	cui.startup();
});

