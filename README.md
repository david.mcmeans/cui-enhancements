# cUI Enhancements

Improves a popular electronic research administration web application.

Adds post page-load tweaks, corrections and improvements to the Cayuse electronic research 
administration (eRA) subscription-based web application at cayuse.com. These improvements 
include items such as restoring the category counts on the Human Ethics dashboard which 
were recently removed to preserve page load performance. Improvements are limited to what 
can be shown on the current page in Cayuse. No external resources are used or required. 
As the Cayuse eRA application changes, the active list of improvements changes as well. 
This extension is useful only for those who have an active subscription to the Cayuse platform.

## Install

You may install this Chrome Extension by pointing your Chrome browser to [cUI Enhancements](
https://chrome.google.com/webstore/detail/cui-enhancements/dmldckalpfamhagebhldmobifanahccm)

## Manifest

The src folder contains all the code used for the Chrome extension. You may use this to 
reivew the code and install the extension locally. 

## Use

No warranty is provided for this Chrome extension. If it does not work as desired, the only 
rememdy is to stop using it. To do so, simply disable or remove the extension.

## Contact

For more information, contact David S. McMeans david.mcmeans@live.com